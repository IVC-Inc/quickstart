import { Component } from '@angular/core';

import { ClickMeComponent } from './click-detail.component';


@Component({
    selector: 'my-app',
	template:`
  		<h2>{{title}}</h2>
  		<click-me></click-me>
  	`,
  	directives: [ClickMeComponent],
})
export class AppComponent {
	title = 'Chat Window';
}
