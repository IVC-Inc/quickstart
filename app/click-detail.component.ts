import { Component } from '@angular/core';
import { PageScroll } from 'ng2-page-scroll/ng2-page-scroll';

@Component({
	selector: 'click-me',
	template: `
		<div>
			<div class="div__listHolder">
				<ul><li *ngFor="let mess of messages">{{mess}}</li></ul>
				<a pageScroll href="#bottom"></a>
			</div>
			<input #newMess placeholder="Start Typing..."/>
			
			<button (click)="onClickMe(newMess.value)">Send</button>	
		</div>
	`,
	directives: [PageScroll]
})
export class ClickMeComponent {

	messages: Array<string>;
	constructor () {
		this.messages=[];
	}

	onClickMe(newMess:string) {
	  if (newMess) {
	  	this.messages.push(newMess);
	  }
	}	
}